import time
from collections import OrderedDict
from functools import lru_cache


def decorator_factory(arg1):
    def decorator(f):
        def func(*a, **kw):
            print("Дополнительный аргумент из фабрики декораторов", arg1)
            return f(*a, **kw)
        return func
    return decorator


@decorator_factory("ping")
def test():  # test = decorator_factory(test)
    pass


def cache(size=3):  # Кэширование реализованное при помощи фабрики декораторов
    def cache_decorator(fn):
        cache_dict = OrderedDict()
        def wrapper(*args):
            if args not in cache_dict:
                if len(cache_dict) == size:
                    cache_dict.popitem(last=True)
                res = fn(*args)
                cache_dict[args] = res
            return cache_dict[args]
        return wrapper
    return cache_decorator


@cache()  # == @lru_cache()
def my_sleep():
    time.sleep(5)


if __name__ == "__main__":
    t0 = time.time()
    my_sleep()
    my_sleep()
    print(t0 - time.time())
